import React, { useEffect, useRef } from 'react'
import { GameModel, MultiGameModel } from './SnakeModel'
import { distance } from './snakeUtils'

export const GameRenderer: React.FC<{
    game: MultiGameModel;
    renderWidth: number;
    renderHeight: number;
}> = React.memo(({
    game,
    renderHeight,
    renderWidth
}) => {
    const canvasRef = useRef<HTMLCanvasElement>()
    const handle = useRef<number>()
    const gameRef = useRef<MultiGameModel>(game)
    gameRef.current = game

    const render = () => {
        const game = gameRef.current

        const context = canvasRef.current?.getContext('2d')
        context.clearRect(0, 0, renderWidth, renderHeight)

        const cellSize = Math.min(renderWidth / game.width, renderHeight / game.height)

        context.save()
        context.beginPath()
        context.lineWidth = 1
        context.strokeStyle = 'red'
        context.fillStyle = 'white'

        context.moveTo(0, 0)
        context.lineTo(0, renderHeight)
        context.lineTo(renderWidth, renderHeight)
        context.lineTo(renderWidth, 0)
        context.lineTo(0, 0)

        context.fill()
        context.stroke()
        context.restore()

        context.save()
        context.beginPath()
        //context.lineTo(100, 100) 
        context.lineWidth = 0.5
        context.strokeStyle = 'black'
        for (let counter = 1; counter < game.width; counter++) {
            let lineAddress = counter * cellSize
            context.moveTo(0, lineAddress)
            context.lineTo(renderWidth, lineAddress)
        }
        for (let counter = 1; counter < game.height; counter++) {
            let lineAddress = counter * cellSize
            context.moveTo(lineAddress, 0)
            context.lineTo(lineAddress, renderHeight)
        }

        context.stroke()
        context.restore()
        
        game.foods.forEach(food => {
            context.beginPath()            
            context.strokeStyle = 'green'
            //context.arc(food.cell[0] * cellSize + cellSize / 2, food.cell[1] * cellSize + cellSize / 2, cellSize/4, 0, 2 * Math.PI, true)
            context.moveTo(food.cell[0] * cellSize + cellSize / 2, food.cell[1] * cellSize + cellSize / 2)
            context.lineTo(food.cell[0] * cellSize + cellSize / 2, food.cell[1] * cellSize + cellSize / 2)
            context.lineWidth = cellSize/2
            context.stroke()
        })        

        game.snakes.forEach(snake=>{
            context.beginPath()        
            snake.cells.forEach(([x, y], index, array) => {
                const prevCell = array[index-1]
                if(prevCell && distance(prevCell, [x,y]) > 1.1) {
                    context.stroke()
                    context.beginPath()
                }
                context.lineTo(x * cellSize + cellSize/2, y * cellSize + cellSize/2)
                context.lineWidth = cellSize * 0.8
                context.strokeStyle = 'orange'
                context.lineJoin = 'round'
                context.lineCap = 'round'
            })
            context.stroke()
        })
        
        handle.current = requestAnimationFrame(render)
    }

    useEffect(() => {
        render()

        return () => {
            cancelAnimationFrame(handle.current)
        }
    }, [])

    return <canvas
        width={renderWidth}
        height={renderHeight}
        style={{
            width: renderWidth,
            height: renderHeight
        }}
        ref={canvasRef} />
})
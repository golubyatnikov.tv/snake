import React from 'react'
import { Link } from 'react-router-dom'

export const MainMenu: React.FC = React.memo(() => {
    return <div style={{display: 'flex', flexDirection: 'column'}}>
        {/* <Link to='/single'>Один игрок</Link> */}
        <Link to='/cooperate'>Два игрока</Link>
    </div>
})
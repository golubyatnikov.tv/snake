export type GameModel = {
    paused: boolean;
    width: number;
    height: number;

    foods: Food[];

    //snake: Snake;
    snakes: Snake[];
    
    maxLength: number;
}

export type MultiGameModel = {
    paused: boolean;
    width: number;
    height: number;

    foods: Food[];
    
    snakes: Snake[];
    
    maxLength: number;
}

export type Food = {
    cell: Cell;
    hasLive: boolean;
}

export type Snake = {   
    id?: string; 
    newDirection?: Cell;
    lives: number;
    reverse: boolean;
    eatenFood: number;
    cells: Cell[];
    direction: Cell; // [-1, 0]; [0, 0]; [1, 0]
}

export type Cell = [number, number]

export type GameInfo = {
    id: string;
    players: {
        id: string;
        ready?: boolean;
    }[];
}
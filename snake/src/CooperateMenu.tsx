import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { GameInfo } from './SnakeModel'
import { useSocket } from './SocketProvider'

export const CooperateMenu: React.FC = React.memo(() => {
    const socket = useSocket()
    const [games, setGames] = useState<GameInfo[]>([])
    const history = useHistory()

    useEffect(() => {
        socket.on('lobbyGames', setGames)

        socket.once('gameJoined', (info) => {
            history.push('/cooperate/game/' + info.id, info)
        })
    }, [])

    return <div style={{ display: 'flex', flexDirection: 'column' }}>
        <button onClick={() => socket.emit('createGame')}>Создать</button>

        <div>
            {games.map(g => <div key={g.id}>
                <div>{g.id}</div>
                <button onClick={() => socket.emit('joinGame', g.id)}>Присоединиться</button>
            </div>)}
        </div>
    </div>
})
import React from 'react'

export const PlayerStats: React.FC<{
    lifes: number;
    eatenFood: number;
}> = React.memo(({
    lifes,
    eatenFood
})=>{
    return <div style={{display: 'flex', justifyContent: 'space-between'}}>
        <div>Очки: {eatenFood}</div>
        <div>Жизни: {lifes}</div>
    </div>
})
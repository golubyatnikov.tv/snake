import { Cell, GameModel, MultiGameModel } from "./SnakeModel";

export function add(c1: Cell, c2: Cell) {
    return [c1[0] + c2[0], c1[1] + c2[1]] as Cell
}

export function equals(c1: Cell, c2: Cell) {
    return c1[0] == c2[0] && c1[1] == c2[1]
}

export function distance([x1, y1]: Cell, [x2, y2]: Cell) {
    return Math.sqrt(((x2-x1)**2)+((y2-y1)**2))
    //Math.abs((x1 - x2)
    //(y1 - y2)
}

export function randomFreeCell(game: MultiGameModel): Cell {
    while (true) {
        let variant = generateRandomCell(game)

        let snakeCell: Cell        
        game.snakes.find(s=>{
            const cell = s.cells.find(cell => equals(variant, cell))
            if(cell) {
                snakeCell = cell
                return true
            }
            return false
        })                    
        const food = game.foods.find(food => equals(variant, food.cell));
        
        if(!snakeCell && !food)
            return variant
    }
}

export function generateRandomCell(game: { width: number, height: number }): Cell {
    let variant = [Math.floor(Math.random() * game.width), Math.floor(Math.random() * game.height)] as Cell
    return variant
}
import React from 'react'
import { Link } from 'react-router-dom'
import { GameModel } from './SnakeModel'

export const GameOverMenu: React.FC<{
    game: GameModel;
}> = React.memo(({
    game
}) => {
    return <div style={{
        position: 'fixed',
        inset: 0,
        background: 'white',
        opacity: 0.5,
        //backdrop-filter: 'blur(3px)'      
    }}>
        {/* <div>Набрано очков: {game.snake.eatenFood}</div> */}
        <Link to='/'>
            <button>Завершить</button>
        </Link>
    </div>
})
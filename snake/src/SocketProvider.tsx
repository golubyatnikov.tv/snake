import React, { createContext, useContext, useMemo } from 'react'
import { io, Socket } from 'socket.io-client'

const SocketContext = createContext<Socket>(null)

export const useSocket = () => useContext(SocketContext)
 
export const SocketProvider: React.FC = React.memo(({children})=>{
    const socket = useMemo(()=> io(import.meta.env.DEV ? 'ws://localhost:9100' : null, {
        transports: ['websocket']        
    }), [])

    return <SocketContext.Provider value={socket}>
        {children}
    </SocketContext.Provider>
})
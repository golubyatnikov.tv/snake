import React, { useEffect, useRef } from "react";
import { Cell, GameModel } from "./SnakeModel";
import { add, equals, randomFreeCell } from "./snakeUtils";

// export const GameLoop: React.FC<{
//     game: GameModel;
//     refresh(): void;
// }> = React.memo(({
//     game,
//     refresh
// }) => {

//     const handle = useRef<any>()
//     const baseFps = 3
//     let fps = baseFps
//     const keyboardState = useRef<string[]>([])

//     const update = () => {
//         const key = keyboardState.current.pop()
//         const newDirection = directionByKey[key]
//         // вычисляется направление на основе последней нажатой клавиши
//         if (key && newDirection) {
//             game.snake.direction = newDirection
//         }

//         if (!equals(game.snake.direction, [0, 0])) {
//             // вычисляется новое положение головы змейки
//             let newHead = add(game.snake.cells[0], game.snake.direction)

//             if (equals(newHead, game.snake.cells[1])) {
//                 game.snake.cells.reverse()
//                 newHead = add(game.snake.cells[0], game.snake.direction)
//             }

//             const w = game.width
//             const h = game.height
//             if (newHead[0] >= w) newHead[0] = 0
//             if (newHead[0] < 0) newHead[0] = w
//             if (newHead[1] >= h) newHead[1] = 0
//             if (newHead[1] < 0) newHead[1] = h

//             const snakeIndex = game.snake.cells.findIndex(snake_cell => equals(newHead, snake_cell))

//             if (snakeIndex >= 0) {
//                 game.snake.lives -= 1
//                 const new_cell = randomFreeCell(game)
//                 game.snake.cells = [new_cell, add(new_cell, [0, 1]), add(new_cell, [0, 2])]
//                 game.snake.direction = [0, 0]

//                 if (game.snake.lives <= 0) {

//                 }

//             }
//             else {
//                 // находим индекс еды в новом положении головы змейки
//                 const foodIndex = game.foods.findIndex(food => equals(newHead, food.cell))

//                 if (foodIndex < 0) // если не нашли
//                     game.snake.cells.pop() // то удаляем хвост
//                 else { // если нашли
//                     game.snake.eatenFood += 1
//                     game.foods.splice(foodIndex, 1) // удаляем еду
//                     game.foods.push({
//                         cell: randomFreeCell(game),
//                         hasLive: false
//                     }) // генерируем новую еду
//                     // корректируем скорость
//                     fps = baseFps + Math.floor(game.snake.eatenFood / 2) + game.snake.eatenFood % 2
//                 }

//                 // добавляем новую голову
//                 game.snake.cells.unshift(newHead)
//             }


//             // очищаем массив нажатых клавиш
//             keyboardState.current = []

//             refresh()
//         }

//         handle.current = setTimeout(update, 1000 / fps)
//     }

//     useEffect(() => {
//         const onKeyDown = (e: KeyboardEvent) => {
//             if (e.key == 'Escape') {
//                 game.paused = true
//                 refresh()
//             }
//             else {
//                 keyboardState.current.push(e.code)
//             }
//         }

//         if (!game.paused) {
//             update()
//             addEventListener('keydown', onKeyDown)
//         }

//         return () => {
//             clearTimeout(handle.current)
//             removeEventListener('keydown', onKeyDown)
//         }
//     }, [game.paused])

//     return null
// })

export const directionByKey: Record<string, Cell> = {
    'KeyW': [0, -1],
    'KeyA': [-1, 0],
    'KeyD': [1, 0],
    'KeyS': [0, 1],
    'ArrowUp': [0, -1],
    'ArrowLeft': [-1, 0],
    'ArrowRight': [1, 0],
    'ArrowDown': [0, 1],
}
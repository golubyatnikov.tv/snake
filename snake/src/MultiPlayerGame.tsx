import React, { useEffect, useState } from 'react'
import { useLocation, useParams } from 'react-router-dom'
import { directionByKey } from './GameLoop'
import { GameOverMenu } from './GameOverMenu'
import { GameRenderer } from './GameRenderer'
import { PauseMenu } from './PauseMenu'
import { PlayerStats } from './PlayerStats'
import { GameInfo, GameModel, MultiGameModel } from './SnakeModel'
import { useSocket } from './SocketProvider'

export const MultiPlayerGame: React.FC = React.memo(() => {
    const gameId = useParams<{ id: string }>()
    const [info, setInfo] = useState<GameInfo>(useLocation().state as any)
    const [game, setGame] = useState<MultiGameModel>()
    const socket = useSocket()

    const iAmReady = info.players.find(p => p.id == socket.id)?.ready

    useEffect(() => {
        socket.on('gameUpdated', setInfo)
        socket.on('gameStarted', setGame)      
        socket.on('gameDataUpdated', (g) => {
            console.log(g)
            setGame(g)
        })
    }, [])

    useEffect(() => {
        if(!game)
            return

        window.addEventListener('keydown', (e) => {
            const direction = directionByKey[e.key]
            socket.emit('snakeControl', {
                direction
            })
        })
    }, [game])

    return <>
        {!game &&
            <div>
                {info.players.map(p => <div key={p.id}>
                    {p.id} - {p.ready ? 'Готов' : 'Не готов'}
                </div>)}
                {<button disabled={iAmReady} onClick={() => socket.emit('iAmReady')}>Начать</button>}
            </div>
        }
        {game && <>
            <div style={{ margin: 10, width: 500 }}>
                {game.snakes.map((snake)=>{
                    <PlayerStats lifes={snake.lives} eatenFood={snake.eatenFood} />
                })}                
                {/* <GameLoop game={game} refresh={() => setGame({ ...game })} /> */}
                <GameRenderer game={game} renderWidth={500} renderHeight={500} />
            </div>
            {
                game.paused && <PauseMenu resume={() => {
                    setGame({ ...game, paused: false })
                }} />
            }
            {/* {!game.snake.lives && <GameOverMenu game={game} />} */}
        </>}
    </>
})
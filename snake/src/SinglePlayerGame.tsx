import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
// import { GameLoop } from './GameLoop'
import { GameOverMenu } from './GameOverMenu'
import { GameRenderer } from './GameRenderer'
import { PauseMenu } from './PauseMenu'
import { PlayerStats } from './PlayerStats'
import { GameModel, MultiGameModel } from './SnakeModel'

export const SinglePlayerGame: React.FC = React.memo(() => {
    const [game, setGame] = useState<MultiGameModel>({
        paused: false,
        width: 20,
        height: 20,
        maxLength: 231,
        snakes: [{
            cells: [[5, 5], [5, 6], [6, 6]],
            eatenFood: 0,
            lives: 3,
            reverse: false,
            direction: [1, 0]
        }],
        foods: [{
            cell: [0, 0],
            hasLive: false,
        }, {
            cell: [10, 10],
            hasLive: false,
        }]
    })

    return <>
        <div style={{ margin: 10, width: 500 }}>
            {/* <PlayerStats lifes={game.snake.lives} eatenFood={game.snake.eatenFood} /> */}
            {/* <GameLoop game={game} refresh={() => setGame({ ...game })} /> */}
            <GameRenderer game={game} renderWidth={500} renderHeight={500} />
        </div>
        {
            game.paused && <PauseMenu resume={() => {
                setGame({ ...game, paused: false })
            }} />
        }
        {/* {!game.snake.lives && <GameOverMenu game={game} />} */}
    </>
})
import './App.css'
import { Route, Switch } from 'react-router-dom'
import { MainMenu } from './MainMenu'
// import { SinglePlayerGame } from './SinglePlayerGame'
import { CooperateMenu } from './CooperateMenu'
import { MultiPlayerGame } from './MultiPlayerGame'
import { SocketProvider } from './SocketProvider'

function App() {
  return (
    <Switch>
      <Route path='/' exact component={MainMenu} />
      {/* <Route path='/single' exact>
        <SinglePlayerGame />
      </Route> */}
      <Route path='/cooperate'>
        <SocketProvider>
          <Switch>
            <Route path='/cooperate' exact>
              <CooperateMenu />
            </Route>
            <Route path='/cooperate/game/:id' exact>
              <MultiPlayerGame />
            </Route>
          </Switch>
        </SocketProvider>
      </Route>
    </Switch>
  )
}

export default App

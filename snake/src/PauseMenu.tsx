import React from "react"

export const PauseMenu: React.FC<{
    resume(): void;
}> = React.memo(({
    resume
})=>{
    return <div style={{
        position: 'fixed',
        inset: 0,
        background: 'white',
        opacity: 0.5,
        //backdrop-filter: 'blur(3px)'      
    }}>
        <button onClick={resume}>Продолжить</button>
    </div>
})
import cors from 'cors';
import express from 'express'
import http from 'http'
import { Server } from "socket.io";
import { Cell, GameInfo, MultiGameModel } from '../snake/src/SnakeModel';
import { generateRandomCell } from '../snake/src/snakeUtils';
import { createGameLoop } from './createGameLoop';
import { ServerGame } from './types';

const PORT = process.env.PORT || 9100

const app = express()
app.use(cors())
app.use(express.static('public'))

const server = http.createServer(app);

const io = new Server(server);

const games: Record<string, ServerGame> = {}
let gameIdCounter: number = 0

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
  console.log('a user connected', socket.id);

  socket.join('looby')
  socket.emit('lobbyGames', Object.values(games).map(g => g.info))

  socket.on('disconnect', (reason) => {
    console.log('a user disconnected', reason, socket.id);
  })

  socket.on('createGame', () => {
    const id = (++gameIdCounter).toString()
    const info: GameInfo = {
      id: id,
      players: [{
        id: socket.id
      }]
    }
    games[id] = {
      info: info,
      data: null,
      gameLoop: null
    }

    socket.join(id)

    io.to('looby').emit('lobbyGames', Object.values(games).map(g => g.info))

    socket.emit('gameJoined', info)
  })

  socket.on('joinGame', id => {
    const game = games[id]
    game.info.players.push({
      id: socket.id
    })

    socket.join(id)

    socket.emit('gameJoined', game.info)
    io.to(id).emit('gameUpdated', game.info)
  })

  socket.on('iAmReady', () => {
    const game = Object.values(games).find(g => g.info.players.some(p => p.id == socket.id))
    const player = game.info.players.find(p => p.id == socket.id)
    player.ready = true
    io.to(game.info.id).emit('gameUpdated', game.info)

    if (game.info.players.every(p => p.ready)) {
      game.data = {
        width: 20,
        height: 20,
        foods: [{
          hasLive: false,
          cell: generateRandomCell({width: 20, height: 20})
        }, {
          hasLive: false,
          cell: generateRandomCell({width: 20, height: 20})
        }],
        maxLength: 10,
        paused: false,
        snakes: game.info.players.map((p, i)=>({ 
          id: p.id,
          cells: [[10 + i*2, 10]], 
          direction: [0,0], 
          lives: 3, 
          eatenFood: 0, 
          reverse: false 
        }))
      } as MultiGameModel

      const gameLoop = createGameLoop(game.data, () => io.to(game.info.id).emit('gameDataUpdated', game.data))
      game.gameLoop = gameLoop
      game.gameLoop.start()
      
      io.to(game.info.id).emit('gameStarted', game.data)
    }
  })

  socket.on('snakeControl', (controlData: { direction: Cell }) => {
    const game = Object.values(games).find(g => g.info.players.some(p => p.id == socket.id))
    //const player = game.info.players.find(p => p.id == socket.id)    
    const snake = game.data.snakes.find(s=>s.id == socket.id)
    snake.newDirection = controlData.direction
  })

  // createGame  
  // > lobbyGames  
  // joinGame
  // > gameJoined
  // > gameUpdated 
  // iAmReady
  // > gameUpdated
  // > gameStarted
  // > gameDataUpdated
  // snakeControl
});

server.listen(PORT, () => {
  console.log(`listening on ${PORT}:PORT`);
});
import { GameInfo, GameModel, MultiGameModel } from '../snake/src/SnakeModel'
import { GameLoop } from './createGameLoop'

export type ServerGame = {
    info: GameInfo;
    data: MultiGameModel;
    gameLoop: GameLoop;
}
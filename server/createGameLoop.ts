import { GameModel, MultiGameModel } from "../snake/src/SnakeModel";
import { add, equals, randomFreeCell } from '../snake/src/snakeUtils'

export function createGameLoop(game: MultiGameModel, refresh: (game: MultiGameModel) => void) {

    let handle: any = null
    const baseFps = 3
    let fps = baseFps
    //game.

    const update = () => {        
        game.snakes.forEach(snake => {
            const newDirection = snake.newDirection;

            if(newDirection)
                snake.direction = newDirection

            if (!equals(snake.direction, [0, 0])) {
                // вычисляется новое положение головы змейки
                let newHead = add(snake.cells[0], snake.direction)
    
                if(snake.cells.length > 1){
                    if (equals(newHead, snake.cells[1])) {
                        snake.cells.reverse()
                        newHead = add(snake.cells[0], snake.direction)
                    }
                }
    
                const w = game.width
                const h = game.height
                if (newHead[0] >= w) newHead[0] = 0
                if (newHead[0] < 0) newHead[0] = w
                if (newHead[1] >= h) newHead[1] = 0
                if (newHead[1] < 0) newHead[1] = h
    
                const snakeIndex = snake.cells.findIndex(snake_cell => equals(newHead, snake_cell))
    
                if (snakeIndex >= 0) {
                    snake.lives -= 1
                    const new_cell = randomFreeCell(game)
                    snake.cells = [new_cell, add(new_cell, [0, 1]), add(new_cell, [0, 2])]
                    snake.direction = [0, 0]
    
                    if (snake.lives <= 0) {
    
                    }
    
                }
                else {
                    // находим индекс еды в новом положении головы змейки
                    const foodIndex = game.foods.findIndex(food => equals(newHead, food.cell))
    
                    if (foodIndex < 0) // если не нашли
                        snake.cells.pop() // то удаляем хвост
                    else { // если нашли
                        snake.eatenFood += 1
                        game.foods.splice(foodIndex, 1) // удаляем еду
                        game.foods.push({
                            cell: randomFreeCell(game),
                            hasLive: false
                        }) // генерируем новую еду
                        // корректируем скорость
                        fps = baseFps + Math.floor(snake.eatenFood / 2) + snake.eatenFood % 2
                    }
    
                    // добавляем новую голову
                    snake.cells.unshift(newHead)
                }            
            }
        })
                
        refresh(game)

        handle = setTimeout(update, 1000 / fps)
    }

    return {
        start() {
            update()
        },
        stop() {
            // TODO
        }
    }
}

export type GameLoop = ReturnType<typeof createGameLoop>;
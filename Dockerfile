FROM node:alpine
ENV NODE_ENV=production
WORKDIR /app-src/snake
COPY snake/ /app-src/snake
WORKDIR /app-src/server
COPY server ./
RUN yarn --silent --production=false
#RUN ls /app-src/server/node_modules -1
RUN yarn tsc
RUN mv build/ /app
RUN mv node_modules/ /app
WORKDIR /app-src/snake
RUN yarn --silent --production=false
RUN yarn build
RUN mv dist/ /app/server/public
#RUN ls /app/server
WORKDIR /app/server
EXPOSE 3000
RUN chown -R node /app
USER node
CMD ["node", "server.js"]
